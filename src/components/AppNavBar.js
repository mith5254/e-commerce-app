import { Container, Navbar, Nav, Tooltip, OverlayTrigger } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import { useContext, useState } from "react";
import UserContext from "../UserContext";
import Cart from "./Cart";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faRightFromBracket} from '@fortawesome/free-solid-svg-icons'


export default function AppNavBar(){
    const { user } = useContext(UserContext);

    return (
        <Navbar bg="light" expand="lg">
            <Container fluid>
                <Navbar.Brand as={NavLink} to="/" className="fancy">7nBen</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav"/>
                <Navbar.Collapse id="basic-navbar-nav" >
                    <Nav className="ms-auto">
                        <Nav.Link as={NavLink} to="/">Home</Nav.Link>
                        {
                            (user.id && user.isAdmin) ?
                                <>
                                    <Nav.Link as={NavLink} to="/dashboard">Dashboard</Nav.Link>
                                </>                                
                            :
                                <Nav.Link as={NavLink} to="/products">Products</Nav.Link>
                        }
                        {
                            !user.isAdmin && localStorage.getItem('token') && <Cart/>
                        }
                        {
                            (user.id) ?
                                <>
                                    
                                    <Nav.Link as={NavLink} to="/logout">
                                        <OverlayTrigger
                                            key= "bottom"
                                            placement= "bottom"
                                            overlay={
                                                <Tooltip className={`nav-tooltip log-out`}>
                                                    Logout
                                                </Tooltip>
                                            }
                                            >
                                            <FontAwesomeIcon icon={faRightFromBracket}/>
                                        </OverlayTrigger>
                                    </Nav.Link>
                                </>
                            :
                                <>
                                    <Nav.Link as={NavLink} to="/login">Login</Nav.Link>
                                    <Nav.Link as={NavLink} to="/register">Register</Nav.Link>
                                    
                                </>
                        }

                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    )







}