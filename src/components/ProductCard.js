import { Card, Col, Row, ListGroup, Container } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function ProducCard({ product }){
    const { _id, name, description, price, imgLink  } = product;

    return (
        <Col xs={12} md={3}>
            <Card className="mt-3 productCards">
                {
                    (imgLink) ?
                        <Card.Img variant="top" src={`${imgLink}`} className="prodImg border"/>
                    :
                        <Card.Img variant="top" src="https://svgur.com/i/AY2.svg" className="prodImg border p-1"/>
                }
                <Card.Body>
                    <Card.Title >{ name }</Card.Title>
                    <Card.Subtitle className="mt-3">Description</Card.Subtitle>
                    <Card.Text>{ description }</Card.Text>
                </Card.Body>
                <Container fluid className="">
                    <Card.Subtitle>Price:</Card.Subtitle>
                    <Card.Text className="m-0">Php { price }</Card.Text>    
                    <Link className="btn btn-primary buttons" to={`/products/${_id}`}>Details</Link>
                </Container>
            </Card>
        </Col> 
    )

}

