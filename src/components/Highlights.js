import { Carousel, Row, Col, Container, Card } from "react-bootstrap";


export default function Highlights() {
    return (
            <Container className="border border-muted">
                <Row>
                    <Col sm={12} md={9} className="d-block d-md-inline p-0">
                        <Carousel>
                            <Carousel.Item interval={1000}>
                                <img
                                className="d-block w-100"
                                src="https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/f19216118883485.6092425f52680.jpg"
                                alt="Nike Collection"
                                />
                                <Carousel.Caption>
                                <h3>Nike Collection</h3>
                                <p>Buy or Windowshop our latest line of Nike Products</p>
                                </Carousel.Caption>
                            </Carousel.Item>
                            <Carousel.Item interval={500}>
                                <img
                                className="d-block w-100"
                                src="https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/d80af4118883485.6092425f50c79.jpg"
                                alt="Samsung Gadgets"
                                />
                                <Carousel.Caption>
                                <h3>Samsung</h3>
                                <p>Take a look at the latests of Samsung's Gadgets</p>
                                </Carousel.Caption>
                            </Carousel.Item>
                            <Carousel.Item>
                                <img
                                className="d-block w-100"
                                src="https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/e534f6118883485.6092425f51d79.jpg"
                                alt="Third slide"
                                />
                                <Carousel.Caption>
                                <h3>ROLEX</h3>
                                <p>Gift or Self Love, Rolex.</p>
                                </Carousel.Caption>
                            </Carousel.Item>
                        </Carousel>
                    </Col>
                    <Col className="p-0">
                        <Card className="m-0 p-0">
                            <Card.Img variant="top" src="https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/e45584118883485.6092425f51822.jpg" className="m-0 p-0"/>
                        </Card>
                        <Card className="m-0 p-1">
                            <Card.Img variant="bottom" src="https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/e4c474172154023.647a1acd80251.jpg" className="p-0 m-0"/>
                        </Card>
                        <Card className="m-0 p-0">
                            <Card.Img variant="bottom" src="https://mir-s3-cdn-cf.behance.net/project_modules/max_1200/0b212f118883485.6092425f50616.jpg" className="p-0 m-0"/>
                        </Card>
                    </Col>
                </Row>
            </Container>
    )
}