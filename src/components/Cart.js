import { useState, useContext, useEffect } from "react";
import { Offcanvas, Button, OverlayTrigger, Tooltip, Container, Card, Col, Row } from "react-bootstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {faCartShopping} from '@fortawesome/free-solid-svg-icons';
import UserContext from "../UserContext";
import { Link } from "react-router-dom";


import CartItems from "./CartItems";

export default function Cart(){
  


    // CartShow/Hide Handler Start
    const [ show, setShow ] = useState(false)

    const handleShow = () => {
        setShow(true);
    }

    const handleClose = () => {
        setShow(false);
    }
    // CartShow/Hide Handler End


    return (
        <div>
             <OverlayTrigger
                key= "bottom"
                placement= "bottom"
                overlay={
                    <Tooltip className={`nav-tooltip`}>
                        My Cart
                    </Tooltip>
                }
                >
                <Button variant="muted" onClick={handleShow} className="border-0 p-0 p-md-2"><FontAwesomeIcon icon={faCartShopping}/></Button>
            </OverlayTrigger>
            <Offcanvas show= {show} onHide = {handleClose} placement="end">
                <Offcanvas.Header  className="text-center" closeButton>
                    <Offcanvas.Title className="mx-auto">My Cart</Offcanvas.Title>
                </Offcanvas.Header>
                <Offcanvas.Body>
                        <CartItems/>
                </Offcanvas.Body>
            </Offcanvas>
        </div>

    )
}
