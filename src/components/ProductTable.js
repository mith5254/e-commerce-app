import { Button, Table } from "react-bootstrap";
import { Link } from "react-router-dom";
import Swal from "sweetalert2";
import { Navigate, useNavigate} from "react-router-dom";
import { useEffect, useState } from "react";


export default function ProductTable({product}){


    const status = product.isActive.toString().toUpperCase();

    const token = localStorage.getItem('token');
    const navigate = useNavigate();

    const [update, setUpdate] = useState(false);

    const { _id, name, description, price } = product;

    

    // ARCHIVING TeST
function archiveProduct(id){
    fetch(`${process.env.REACT_APP_API_URL}/products/${ id }/archive`, {
        method : "PATCH",
        headers : {
            "Content-Type" : "application/json",
            Authorization : `Bearer ${token}`
        }
    }).then(res => res.json())
        .then(succes => {
            if(succes) {
                Swal.fire({
                    icon: 'success',
                    title : 'Product Deactivation Successful!',
                    text : 'Product is now Archived.'
                });

                setUpdate(true);

            }else{
                Swal.fire({
                    icon : 'error',
                    title : 'Something Went Wrong',
                    text : 'Database might be currently down.'
                })
            }
        })
}
// ARCHIVING TEST END

function activateProduct(id){
    fetch(`${process.env.REACT_APP_API_URL}/products/${ id }/activate`, {
        method : "PATCH",
        headers : {
            "Content-Type" : "application/json",
            Authorization : `Bearer ${token}`
        }
    }).then(res => res.json())
        .then(succes => {
            if(succes) {
                Swal.fire({
                    icon: 'success',
                    title : 'Product Activation Successful!',
                    text : 'Product is now back on stock.'
                });

                setUpdate(true);

            }else{
                Swal.fire({
                    icon : 'error',
                    title : 'Something Went Wrong',
                    text : 'Database might be currently down.'
                })
            }
        })
}




useEffect(() => {
    if(update){
        setUpdate(false);
        setTimeout(() => {
            window.location.reload();
        }, 2000);
    }
}, [update])


    return (
        <tr>
            <td className="tdata">{_id}</td>
            <td className="tdata">{name}</td>
            <td className="tdata">{description}</td>
            <td className="tdata">{price}</td>
            <td className="tdata">
                {
                    (status == "TRUE") ?
                        <span className="text-primary">AVAILABLE</span>
                    :
                        <span className="text-danger">SOLD OUT</span>
                }
            </td>
            <td>
                {
                    (status == "TRUE") ?
                        <Button variant="danger" className="statBtn" onClick={() => archiveProduct(_id)}>Deactivate</Button>
                        /* <Link className="btn btn-danger statBtn" to={`/products/${_id}/archive`}>Deactivate</Link> */
                    :
                        /* <Link className="btn btn-success statBtn" to={`/products/${_id}/activate`}>Activate</Link> */
                        <Button variant="success" className="statBtn" onClick={() => activateProduct(_id)}>Activate</Button>

                }
                <Link className="btn btn-warning statBtn" to={`/products/${_id}`}>Update</Link>
            </td>
            
            
        </tr>
    )

}