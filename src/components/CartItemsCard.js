
import { Card, Col, Row, Container, Form, Button } from "react-bootstrap";
import { useState, useContext, useEffect } from "react";
import Swal from "sweetalert2";
import UserContext from "../UserContext";






export default function CartItemsCard({ product }){
    // Product Related Variables
    const { name, description, price, imgLink, productId } = product;
    const [ quantity, setQuantity ] = useState(1);
    const handleChange = (e) => {
        const newValue = Math.max(Number(e.target.value), 1); // Ensure the value is not below 1
        setQuantity(newValue);
    };

    const token = localStorage.getItem('token');
    const { user } = useContext(UserContext);

    function checkout(prodId){
        console.log(prodId)
        fetch(`${process.env.REACT_APP_API_URL}/users/checkOut`, {
            method : "POST",
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${token}`
            },
            body : JSON.stringify({
                productId : prodId,
                quantity : quantity
            })
        }).then(res => res.json())
            .then(success => {
                if(success) {
                    Swal.fire({
                        icon : 'success',
                        title : 'Product Checkout Successful!',
                        text : 'Product is on process for delivery.'
                    });

                    return true;
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Product Checkout Failed',
                        text : 'Something went wrong or Server is Currently Down.'
                    })
                }
            }).then(success => {
                if(success){
                    setTimeout(() => {
                        removeCartItem(prodId);
                    }, 3000);
                };
            })
            .catch(err => {
                console.log(err)
            });
    }


    // Rmoving items from Cart Variables and Function -- Start

function removeCartItem(prodId){
    const Carts = JSON.parse(localStorage.getItem('Carts'));
    let currentUser = null;

    for(let owner of Carts.UserCarts){
        if(owner.userId === user.id){
            currentUser = owner;
            break;
        }
    }


    for(let product of currentUser.products){
        if(product.productId === prodId){
           currentUser.products.splice(currentUser.products.indexOf(product),1);
           Swal.fire({
                icon: 'success',
                title: 'Product Removed From Cart',
           });

           setTimeout(() => {
                window.location.reload();   
            }, 2000);
        }
    }

    console.log(Carts);

    localStorage.setItem('Carts', JSON.stringify(Carts));

} 

// Rmoving items from Cart Variables and Function -- END

    return(
        <Container>
            <Row className="justify-content-center">
                <Col>
                    <Card className="mt-3 productCards">
                        {
                            (imgLink) ?
                                <Card.Img variant="top" src={`${imgLink}`} className="prodImg"/>
                            :
                                <Card.Img variant="top" src="https://svgur.com/i/AY2.svg" className="prodImg"/>
                        }
                        <Card.Body>
                            <Card.Title >{ name }</Card.Title>
                            <Card.Subtitle className="mt-3">Description</Card.Subtitle>
                            <Card.Text>{ description }</Card.Text>
                        </Card.Body>
                        <Container fluid className="">
                            <Card.Subtitle className="border-top border-muted">Price:</Card.Subtitle>
                            <Card.Text className="m-0 mb-2 ">Php { price }</Card.Text>   
                            <Form.Group controlId="numberInput">
                            <Row className="justify-content-center border-top border-muted">
                                <Col md={3} className="total">
                                    <Card.Subtitle>Total:</Card.Subtitle>
                                    <Card.Text className="m-0">Php { price * quantity }</Card.Text>
                                </Col>
                                <Col md={3} className="total">
                                    <Form.Label className="d-inline-block">Quantity:</Form.Label>
                                    <Form.Control
                                        type="number"
                                        min="1" // Set the minimum value to 1
                                        value={quantity}
                                        onChange={handleChange}
                                        className="qtyInput"
                                    />
                                </Col>
                            </Row>
                            </Form.Group>
                            <Button variant="success" type="submit" id="submitBtn" className=' buttons' onClick={(e) => checkout(productId)}>Checkout</Button>
                            <Button variant="warning" type="submit" id="submitBtn" className=' buttons' onClick={(e) => removeCartItem(productId)}>Remove</Button>
                        </Container>
                    </Card>
                </Col>
            </Row>
        </Container>    
    )

}


{/* <Card className="my-2 text-center">
            {
                (imgLink) ?
                    <Card.Img variant="top" src={`${imgLink}`} className="prodImg border-bottom border-muted"/>
                :
                    <Card.Img variant="top" src="https://svgur.com/i/AY2.svg" className="prodImg border-bottom border-muted"/>
            }
            <Card.Body>
                <Card.Title>{ name }</Card.Title>
                <Card.Subtitle className="mt-3">Description:</Card.Subtitle>
                <Card.Text className="mb-5">{ description }</Card.Text>
                <Card.Subtitle className="border-top border-muted mt-2 pt-3">Price:</Card.Subtitle>
                <Card.Text>Php { price }</Card.Text>
            </Card.Body>
        </Card> */}