import { Button, Col, Container, Row, Image } from "react-bootstrap";

export default function Banner(status){
    if(status.notFound){
        return (
            <Container fluid>
                <Row className="justify-content-center">
                    <Col sm={12} md={3} className="p-5 text-center">
                        <img src="https://media.istockphoto.com/id/525889538/tr/vekt%C3%B6r/404-error.jpg?s=612x612&w=0&k=20&c=cVNeFOpobboPHZ7yg95ovlzH8XQN7MRqKMV9H8ufaaM=" className="notFound"/>
                        <p>Go back to the <a href="/">homepage</a></p>
                    </Col>
                </Row>
            </Container>
        )
    };

    return (
        <Row>
            <Col className="pt-md-5 pt-0 pb-3 text-center">
                <h1 className="fancy mainLogo m-0">7nBen</h1>
                <p className="mainSubtitle">Serving you Soundly 24/7.</p>
                {/* <Button variant="primary">Enroll Now!</Button> */}
            </Col>
        </Row>
    )
    
}