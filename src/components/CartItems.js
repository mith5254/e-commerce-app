import { useState, useContext, useEffect } from "react";
import UserContext from "../UserContext";
import CartItemsCard from "./CartItemsCard";




export default function CartItems(){

    const { user } = useContext(UserContext);
    // getCarts from LocalStorage - JSON OBJECT
    const [ cartStorage, setCartStorage ] = useState({})

    // getAllCarts from storage - ARRAY
    const [ carts, setCarts ] = useState([]);

    // getAllProducts from currentUser's Cart
    const[ cartProducts, setCartProducts ] = useState([]);


    useEffect(() => {
        if(localStorage.getItem('Carts')){
            setCartStorage(JSON.parse(localStorage.getItem('Carts')))
        };
    }, [])


    useEffect(() => {
        
        if(cartStorage){
            setCarts(cartStorage.UserCarts)
        } 
    }, [cartStorage]);


    useEffect(() => {
        if(carts){
            for( let cart of carts){
                if(cart.userId == user.id){
                    setCartProducts(cart.products);
                }
            }
        }
    }, [carts]);

    if(cartProducts){
        const products = cartProducts.map(product => {
            return (
                <CartItemsCard key={ product.productId } product = { product }/>
            )
        })

        return (
            <>
                { products }
            </>
        )
    }
}
