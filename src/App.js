import { useEffect, useState, useContext } from "react";
import { BrowserRouter as Router, Route, Routes, Link } from "react-router-dom";
import { Container, Button, Offcanvas } from "react-bootstrap";

import { UserProvider } from "./UserContext";
import AppNavBar from "./components/AppNavBar";

// PAGES
import Home from "./pages/Home";
import Banner from "./components/Banner";
import Login from "./pages/Login";
import Register from "./pages/Register";
import Products from "./pages/Products";
import ProductView from "./pages/ProductView";
import Logout from "./pages/Logout";
import AdminDashboard from "./pages/AdminDashboard";
import AddProduct from "./pages/AddProduct";
import UpdateProduct from "./pages/UpdateProduct";
import Checkout from "./pages/Checkout";
import Highlights from "./components/Highlights";
import UserContext from "./UserContext";


import './App.css'


function App() {

  const[user, setUser] = useState({ id: null, isAdmin: null });

  const unsetUser = () => {
    localStorage.removeItem('token');
  };

  useEffect(() => {
    if(localStorage.getItem("token")){
      fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
        headers : {
          "Content-Type" : "application/json",
          Authorization : `Bearer ${ localStorage.getItem("token") }`
        }
      }).then(res => res.json())
          .then(data => {
            // console.log(data);
            setUser({
              id : data._id,
              isAdmin : data.isAdmin
            })
          });
    }
    
    
  }, []);


  

  return (
    <>
    <UserProvider value = {{ user, setUser, unsetUser }}>
      <Router>
        <AppNavBar/>
        
          <Routes>
            <Route path="/" element={<Home/>}/>
            <Route path="/login" element={<Login/>}/>
            <Route path="/logout" element={<Logout/>}/>
            <Route path="/register" element={<Register/>}/>
            <Route path="*" element = { <Banner notFound={true} />} />
            {
              (user.isAdmin) ?
                <>
                  <Route path="/products/:productId" element={<UpdateProduct/>}/>
                  <Route path="/dashboard" element={<AdminDashboard/>}/>
                  <Route path="/addNewProduct" element={<AddProduct/>}/>
                </>
              :
                <>
                  <Route path="/checkout/:productId" element={<Checkout/>}/>
                  <Route path="/products" element={<Products/>}/>
                  <Route path="/products/:productId" element={<ProductView/>}/>
                </>
            }
          </Routes>
      </Router>
    </UserProvider>
    </>
    
  );
}

export default App;
