import { useState, useEffect, useContext } from "react";
import { Card, Container, Col, Button, Row } from "react-bootstrap";
import { useParams, useNavigate, Link } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";



export default function ProductView(){
    const { user } = useContext(UserContext);
    const navigate = useNavigate();

    const { productId } = useParams();

    const [ name, setName ] = useState('');
    const [ price, setPrice ] = useState(0);
    const [ description, setDescription ] = useState('');
    const [ imgLink, setImgLink ] = useState('');


    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }`).then(res => res.json()).then(data => {

            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
            setImgLink(data.imgLink);
        })
    }, [productId])


    function cart(){
        if(localStorage.getItem('Carts')){
            let Carts = JSON.parse(localStorage.getItem('Carts'));
            let UserCarts = Carts.UserCarts;

            let currentUserCartIndex = null;
            let existing = null;

            for(let cart in UserCarts){
                if(UserCarts[cart].userId === user.id){
                    currentUserCartIndex = cart;
                    break;
                }
            }

            if(currentUserCartIndex !== null){
                let productsList = UserCarts[currentUserCartIndex].products;

                for(let products of productsList){
                    if(products.productId === productId){
                        existing = true;
                        break
                    }
                }

                if(!existing){
                    let newProduct = {
                        productId: productId,
                        name: name,
                        price: price,
                        description: description,
                        imgLink: imgLink
                    };

                    productsList.push(newProduct);

                    Swal.fire({
                        icon: 'success',
                        title: 'Item Added to Cart.',
                        text: 'Check Cart to confirm Item Existence.'
                    })

                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Item Already in Cart.',
                        text: 'Check Cart to confirm Item Existence.'
                    })
                }

            }else{
              let newUser =  {
                    userId: user.id,
                    products: [
                        {
                            productId: productId,
                            name: name,
                            price: price,
                            description: description,
                            imgLink: imgLink
                        }
                    ]
                }

                UserCarts.push(newUser);
            }

            localStorage.setItem('Carts', JSON.stringify(Carts));

        }else{
            let Carts = {
                UserCarts: [
                    {
                        userId: user.id,
                        products: [
                            {
                                productId: productId,
                                name: name,
                                price: price,
                                description: description,
                                imgLink: imgLink
                            }
                        ]
                    }
                ]
            };

            localStorage.setItem('Carts', JSON.stringify(Carts));
        }

    }





    return(
        <Container className="mt-5">
            <Row>
                <Col lg={{span: 6, offset: 3 }} className="text-center">
                    <Card>
                        {
                            (imgLink) ?
                                <Card.Img variant="top" src={`${imgLink}`} className="prodImg border-bottom border-muted"/>
                            :
                                <Card.Img variant="top" src="https://svgur.com/i/AY2.svg" className="prodImg border-bottom border-muted"/>
                        }
                        <Card.Body>
                            <Card.Title>{ name }</Card.Title>
                            <Card.Subtitle className="mt-3">Description:</Card.Subtitle>
                            <Card.Text className="mb-5">{ description }</Card.Text>
                            <Card.Subtitle className="border-top border-muted mt-2 pt-3">Price:</Card.Subtitle>
                            <Card.Text>Php { price }</Card.Text>
                            {
                                (user.id) ?
                                    <>
                                        <Link className="btn btn-success buttons" to={`/checkout/${productId}`}>Buy Now</Link>
                                        {/* <Button variant="success" className="buttons" to={`/checkout/${productId}`}>Buy Now</Button> */}
                                        <Button variant="warning" className="buttons" onClick={cart}>Add to Cart</Button>
                                    </>

                                :
                                    <Link className="btn btn-success btn-block buttons" to="/login">Buy Now</Link>
                                    

                            }
                            <Link className="d-block text-muted mt-3" to="/products">Previous Page</Link>
                        </Card.Body>
                    </Card>
                </Col>
            </Row>
        </Container>
    )


}