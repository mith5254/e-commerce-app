import Banner from "../components/Banner"
import Highlights from "../components/Highlights";
import { Container, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import Cart from "../components/Cart";




export default function Home(){
    return(
        <Container className="text-center mt-0">
            <Banner/>
            <Highlights/>
            <Container className="">
                <Link to={"/products"} className="m-5 btn btn-success">Browse Our Products</Link>
            </Container>
        </Container>
    )
}
