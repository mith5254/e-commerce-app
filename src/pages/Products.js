import { Container, Col, Row } from "react-bootstrap";
import ProductCard from "../components/ProductCard";
import { useEffect, useState } from "react";
import Swal from "sweetalert2";

export default function Products(){
    const [ products, setProducts ] = useState([]);
    const [ productsLoaded, setProductsLoaded ] = useState(false);

    useEffect(() => {
        setProductsLoaded(true);
        fetch(`${process.env.REACT_APP_API_URL}/products/`).then(res => res.json()).then(data => {
            setProducts(data.map(product => {
                // console.log(product._id)
                return(
                    <>
                    <ProductCard key={ product._id } product={ product } />
                    </>
                )
            }))
        })
    }, [])

    useEffect(() => {
        Swal.fire({
            icon: 'info',
            title: 'Products May Take Time To Load.',
            text: 'Fetching all products from the Database',
            timer: 3000
        })
    }, [productsLoaded])


    return (
        <Container fluid className="">
            <Row className="justify-content-center m-0 mb-2 m-md-5" >
                { products }
            </Row>
        </Container>
    )





}