import {Button, Col, Container, Form, Row } from "react-bootstrap";
import { Fragment } from "react";
import { Navigate, useNavigate, Link } from "react-router-dom";
import { useEffect, useState, useContext } from "react";
import Swal from "sweetalert2";
import UserContext from "../UserContext";


export default function Login(){

    const navigate = useNavigate();

    const { user, setUser } = useContext(UserContext);

    const [ email, setEmail ] = useState("");
    const [ password, setPassword ] = useState("");
    const [isActive, setIsActive ] = useState(false);

    function authenticate(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method : "POST",
            headers : {
                "Content-Type" : "application/json"
            },
            body : JSON.stringify({
                email : email,
                password : password
            })
        }).then(res => res.json())
            .then(data => {
                // console.log(data)

                if(typeof data.access !== "undefined"){
                    localStorage.setItem('token', data.access);
                    // console.log(data.access._id);
                    retrievedUserDetails(data.access);

                    Swal.fire({
                        icon: 'success',
                        title: 'Login Successful',
                        text: 'Good to have you back.'
                      });

                    navigate('/login');

                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Authentication Failed',
                        text: 'Please check your Login Credentials and try again.'
                      });
                }
            })

        setEmail("");
        setPassword("");
    }

    const retrievedUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`,{
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${ token }`
            }
        }).then(res => res.json())
            .then(data => {
                // console.log(data);

                setUser({
                    id : data._id,
                    isAdmin : data.isAdmin
                })
            });
    };



    useEffect(() => {
        if((email !== "" & password !== "")){
            setIsActive(true);
        }else{
            setIsActive(false);
        };
    }, [email, password]);

    // console.log(user);

    return(
        (user.id) ?
            <Navigate to="/" />
        :

            <Container fluid className="p-5">
                <Row className="justify-content-center">
                    <Col sm={12} md={3} className="p-5 p-md-3 border rounded ">
                        <Form onSubmit={(e) => authenticate(e)}>
                            <h1 className="text-center mt-1 mb-4 fancy logo">7nBen</h1>
                            <Form.Group controlId="userEmail"  className="mb-2">
                                <Form.Label className="m-0">Email Address</Form.Label>
                                <Form.Control
                                    type="email"
                                    placeholder="Enter Email"
                                    value={ email }
                                    onChange={e => setEmail(e.target.value)}
                                    required
                                />
                            </Form.Group>

                            <Form.Group controlId="userPassword"  className="mb-2 mt-3">
                                <Form.Label className="m-0">Password</Form.Label>
                                <Form.Control
                                    type="password"
                                    placeholder="Enter Password"
                                    value={ password }
                                    onChange={e => setPassword(e.target.value)}
                                    required
                                />
                            </Form.Group>
                            {
                                isActive ?
                                    <Button variant="primary" type="submit" id="submitBtn" className='mt-2'>Login</Button>
                                :
                                    <Button variant="danger" type="submit" id="submitBtn" className='mt-2' disabled>Login</Button>
                            }
                            <Container className="mt-3">
                                <Link to={'/register'}>Don't have an account?</Link>
                            </Container>
                        </Form>
                    </Col>
                </Row>
            </Container>
    )

}


