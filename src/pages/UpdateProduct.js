import { useEffect, useState, useContext } from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { Navigate, useNavigate, useParams} from "react-router-dom";
import { Link } from "react-router-dom";

import Swal from "sweetalert2";


export default function UpdateProduct(){
    const navigate = useNavigate();

    const [name, setName ] = useState("");
    const [description, setDescription] = useState("");
    const [price, setPrice] = useState(0);
    const [ imgLink, setImgLink ] = useState("");

    const {productId} = useParams();

    const token = localStorage.getItem('token');

    const [isActive, setIsActive] = useState(false);


    function updateProduct(e){
        e.preventDefault();
        
        fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }`, {
            method : "PUT",
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${token}`
            },
            body : JSON.stringify({
                name : name,
                description : description,
                price : price,
                imgLink : imgLink
            })
        }).then(res => res.json())
            .then(success =>{
                if(success) {
                    Swal.fire({
                        icon: 'success',
                        title : 'Product Updated Successfully!',
                        text: 'Product info updated.'
                    })
                    navigate("/dashboard");
                }else{
                    Swal.fire({
                        icon : 'error',
                        title : 'Something Went Wrong',
                        text : 'Database might be currently down.'
                    })
                }
            }).catch(err => {
                console.log(err);

                Swal.fire({
                    icon : 'error',
                    title : 'Something Went Wrong',
                    text : 'Product Details : Update Failed'
                });
            });
    }

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }`).then(res => res.json()).then(data => {

            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
            setImgLink(data.imgLink);
        })
    }, [productId])

    useEffect(() => {
        (name !== "" && price !== "" && price >= 1 & description !=="") ?
            setIsActive(true)
        :
            setIsActive(false);
    }, [name, description, price]);

    return(
        <Container fluid className="p-5">
                <Row className="justify-content-center">
                    <Col xs={ 12 } md={ 4 } className="rounded border p-5 p-md-3">
                        <Container fluid>
                            <Form onSubmit={(e) => updateProduct(e)}>
                                <Form.Group controlId="name" className="mb-2">
                                    <Form.Label className="m-0">Name</Form.Label>
                                    <Form.Control
                                        type = "text"
                                        placeholder = "Enter Product Name"
                                        value={ name }
                                        onChange={e => setName(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group controlId="description" className="mb-2">
                                    <Form.Label className="m-0">Description</Form.Label>
                                    <Form.Control
                                        type = "text"
                                        placeholder = "Enter Product Description"
                                        value={ description }
                                        onChange={e => setDescription(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group controlId="imglink" className="mb-2">
                                    <Form.Label className="m-0">Image Link</Form.Label>
                                    <Form.Control
                                        type = "text"
                                        placeholder = "Enter Image link."
                                        value={ imgLink }
                                        onChange={e => setImgLink(e.target.value)}
                                        required
                                    />
                                </Form.Group>
                                
                                <Form.Group controlId="price" className="mb-2">
                                    <Form.Label className="m-0">Price</Form.Label>
                                    <Form.Control
                                        type = "number"
                                        placeholder = "Set Product Price"
                                        value={ price }
                                        onChange={e => setPrice(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                    
                                {
                                    isActive ?
                                        <Button variant="success" type="submit" id="submitBtn" className='mt-2'>Update Product Details</Button>
                                        :
                                        <Button variant="danger" type="submit" id="submitBtn" className='mt-2' disabled>Check Product Details</Button>
                                }
                            </Form>
                            <Link className="d-block text-muted text-center mt-2" to="/dashboard">Previous Page</Link>
                        </Container>
                    </Col>
                </Row>
            </Container>
    )

}
