import { useEffect, useContext, useState } from "react";
import { Button, Col, Card, Container, Form, Row} from "react-bootstrap";
import { Link } from "react-router-dom";
import { useNavigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";


export default function Checkout(){
    const [ id, setId] = useState("");
    const [ name, setName] = useState("");
    const [ price, setPrice ] = useState("");
    const [ description, setDescription ] = useState("");
    const [ imgLink, setImgLink ] = useState('');
    
    const [ quantity, setQuantity ] = useState(1);
    const handleChange = (e) => {
        const newValue = Math.max(Number(e.target.value), 1); // Ensure the value is not below 1
        setQuantity(newValue);
    };

    const { productId } = useParams();
    const token = localStorage.getItem('token');
    const navigate = useNavigate();

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/${ productId }`).then(res => res.json()).then(data => {
            console.log(data._id);
            setName(data.name);
            setDescription(data.description);
            setPrice(data.price);
            setId(data._id);
            setImgLink(data.imgLink);

        })
    }, [productId])



    function checkout(prodId){
        console.log(prodId)
        fetch(`${process.env.REACT_APP_API_URL}/users/checkOut`, {
            method : "POST",
            headers : {
                "Content-Type" : "application/json",
                Authorization : `Bearer ${token}`
            },
            body : JSON.stringify({
                productId : prodId,
                quantity : quantity
            })
        }).then(res => res.json())
            .then(success => {
                if(success) {
                    Swal.fire({
                        icon : 'success',
                        title : 'Product Checkout Successful!',
                        text : 'Product is on process for delivery.'
                    });
                    navigate('/products');
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Product Checkout Failed',
                        text : 'Something went wrong or Server is Currently Down.'
                    })
                }
            }).catch(err => {
                console.log(err)
            });
    }


    return(
        <Container fluid className="p-5">
            <Row className="justify-content-center">
                <Col xs={12} md={3}>
                    <Card className="mt-3 productCards">
                        {
                            (imgLink) ?
                                <Card.Img variant="top" src={`${imgLink}`} className="prodImg"/>
                            :
                                <Card.Img variant="top" src="https://svgur.com/i/AY2.svg" className="prodImg"/>
                        }
                        <Card.Body>
                            <Card.Title >{ name }</Card.Title>
                            <Card.Subtitle className="mt-3">Description</Card.Subtitle>
                            <Card.Text>{ description }</Card.Text>
                        </Card.Body>
                        <Container fluid className="">
                            <Card.Subtitle className="border-top border-muted">Price:</Card.Subtitle>
                            <Card.Text className="m-0 mb-2 ">Php { price }</Card.Text>   
                            <Form.Group controlId="numberInput">
                            <Row className="justify-content-center border-top border-muted">
                                <Col md={3} className="total">
                                    <Card.Subtitle>Total:</Card.Subtitle>
                                    <Card.Text className="m-0">Php { price * quantity }</Card.Text>
                                </Col>
                                <Col md={3} className="total">
                                    <Form.Label className="d-inline-block">Quantity:</Form.Label>
                                    <Form.Control
                                        type="number"
                                        min="1" // Set the minimum value to 1
                                        value={quantity}
                                        onChange={handleChange}
                                        className="qtyInput"
                                    />
                                </Col>
                            </Row>
                            </Form.Group>
                            <Link className="btn btn-warning buttons" to={`/products/${id}`}>Cancel</Link>
                            <Button variant="success" type="submit" id="submitBtn" className=' buttons' onClick={(e) => checkout(productId)}>Checkout</Button>
                        </Container>
                    </Card>
                </Col>
            </Row>
        </Container>
    )


}