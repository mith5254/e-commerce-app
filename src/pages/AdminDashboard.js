import { Container, Col, Row, Tab, Table, Button } from "react-bootstrap";
import ProductCard from "../components/ProductCard";
import { useEffect, useState } from "react";
import ProductTable from "../components/ProductTable";
import { Link } from "react-router-dom";

export default function Products(){
    const [ allProducts, setAllProducts ] = useState([]);

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/products/all/`, {
            headers : {
                Authorization : `Bearer ${ localStorage.getItem('token') }`
            }
        }).then(res => res.json()).then(data => {
            console.log(data)

            setAllProducts(data.map(product => {
                return(
                    <ProductTable key={ product._id } product={ product } />
                )
            }))
        })
    }, [])



    return (
/*         <Container fluid>
            <Row className="justify-content-center mt-3 mb-3" >
                { allProducts }
            </Row>
        </Container> */
        <>
        <Container fluid className="text-center my-3">
            <h1>Admin Dashboard</h1>
            <Link to={"/addNewProduct"} className="btn btn-primary statBtn addNew">Add New Product</Link>
        </Container>
        <Container fluid className=" text-center px-5" >
            <Table striped bordered responsive centered className="">
                <thead>
                    <tr>
                        <th>Product Id</th>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Availability</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {allProducts}
                </tbody>
            </Table>
        </Container>
        </>
    )
}