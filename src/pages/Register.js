import { useEffect, useState, useContext } from "react";
import { Form, Button, Container, Row, Col } from "react-bootstrap";
import { Link, Navigate, useNavigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Register(){
    const navigate = useNavigate();

    const [ firstName, setFirstName] = useState("");
    const [ lastName, setLastName] = useState("");
    const [ email, setEmail ] = useState(""); 
    const [ password1, setPassword1 ] = useState(""); 
    const [ password2, setPassword2 ] = useState("");

    const { user, setUser } = useContext(UserContext);

    const [isActive, setIsActive] = useState(false);

    function registerUser(e){
        e.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/register`,{
            method : "POST",
            headers : {
                "Content-Type" : "application/json"
            },
            body : JSON.stringify({
                firstName : firstName,
                lastName : lastName,
                email : email,
                password : password1
            })
        }).then(res => res.json())
            .then(success => {
                console.log(success);
                if(success) {
                    Swal.fire({
                        icon: 'success',
                        title: 'Registration Successful',
                        text: '7nBen User Account Succesfully Created!'
                    })

                    navigate("/login");
                }else{
                    Swal.fire({
                        icon: 'error',
                        title: 'Something went wrong',
                        text: 'Either a Duplicate Email is Found or Database is currently down.'
                    });
                }
            });

    }

    useEffect(() => {
        // Validation to enable submit button when all fields are populated and both passwords match
        if((email !== "" && password1 !== "" && password2 !== "" && firstName !== "" && lastName !== "") && (password1 === password2)){
            setIsActive(true);
        }else{
            setIsActive(false);
        };
    }, [firstName, lastName, email, password1, password2]);

    return(
        (user.id) ?
            <Navigate to="/" />
        : 
            <Container fluid className="p-5">
                <Row className="justify-content-center">
                    <Col xs={ 12 } md={ 4 } className="rounded border p-5 p-md-3">
                        <Container fluid>
                            <Form onSubmit={(e) => registerUser(e)}>
                                <h1 className="text-center mt-1 mb-4 fancy logo">7nBen</h1>
                                <Form.Group controlId="firstName" className="mb-2">
                                    <Form.Label className="m-0">Firstname</Form.Label>
                                    <Form.Control
                                        type = "text"
                                        placeholder = "Enter Firstname"
                                        value={ firstName }
                                        onChange={e => setFirstName(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group controlId="lastName" className="mb-2">
                                    <Form.Label className="m-0">Lastname</Form.Label>
                                    <Form.Control
                                        type = "text"
                                        placeholder = "Enter Fastname"
                                        value={ lastName }
                                        onChange={e => setLastName(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                <Form.Group controlId="userEmail" className="mb-2">
                                    <Form.Label className="m-0">Email Address</Form.Label>
                                    <Form.Control
                                        type = "email"
                                        placeholder = "Enter Email"
                                        value={ email }
                                        onChange={e => setEmail(e.target.value)}
                                        required
                                    />
                                    <Form.Text className="text-muted">We'll never share your email with anyone</Form.Text>
                                </Form.Group>

                                <Form.Group controlId="password1" className="mb-2">
                                    <Form.Label className="m-0">Password</Form.Label>
                                    <Form.Control
                                        type = "password"
                                        placeholder = "Enter a password"
                                        value={ password1 }
                                        onChange={e => setPassword1(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                                
                                <Form.Group controlId="password2" className="mb-2">
                                    <Form.Label className="m-0">Confirm Password</Form.Label>
                                    <Form.Control
                                        type = "password"
                                        placeholder = "Confirm your password"
                                        value={ password2 }
                                        onChange={e => setPassword2(e.target.value)}
                                        required
                                    />
                                </Form.Group>

                    

                                {
                                    isActive ?
                                        <Button variant="success" type="submit" id="submitBtn" className='mt-2'>Register</Button>
                                        :
                                        <Button variant="danger" type="submit" id="submitBtn" className='mt-2' disabled>Check Account Details</Button>
                                }
                                <Container className="mt-3">
                                    <Link to={'/login'}>Login instead?</Link>
                                </Container>
                            </Form>
                        </Container>
                    </Col>
                </Row>
            </Container>
    )




}